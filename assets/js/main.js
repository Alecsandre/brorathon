var MarathonListPage = {
	init: function() {
		this.$container = $('.marathons-container');
		this.render();
		this.bindEvents();
	},

	render: function() {

	},

	bindEvents: function() {
		$('.btn-validate', this.$container).on('click', function(e) {
			e.preventDefault();

			var self = $(this);
			var url = $(this).attr('href');
			$.getJSON(url, function(result) {
				if (result.success) {
					$('.glyphicon', self).toggleClass("glyphicon-star-empty")
				}
			});

			return false;
		});
	}
};

$(document).ready(function() {
	MarathonListPage.init();
});
