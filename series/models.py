from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify


class Serie(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    synop = models.TextField(null=True, blank=True)
    thumb = models.ImageField(default='default.png')
    nb_eps = models.IntegerField()
    nb_eps_vu = models.IntegerField()
    add_by = models.ForeignKey(User)

    def __str__(self):
        return self.title

    def save(self):
        self.slug = slugify(self.title)
        super().save()

        n_eps = self.episode_set.count()

        if n_eps < self.nb_eps:
            for n in range(self.nb_eps - n_eps):
                i = n_eps + n + 1
                Episode(title=self.title + " " +
                        str(i), num=i, serie=self).save()
        elif n_eps > self.nb_eps:
            for n in range(n_eps - self.nb_eps):
                self.episode_set.get(num=self.nb_eps + n + 1).delete()

    class Meta:
        verbose_name = "Serie"
        verbose_name_plural = "Series"
        ordering = ['-nb_eps_vu', 'title']


class Marathon(models.Model):
    serie = models.ForeignKey(Serie, on_delete=models.CASCADE)
    date = models.DateTimeField(unique=True)
    place = models.CharField(max_length=150)
    is_validate = models.BooleanField(default=False)
    add_by = models.ForeignKey(User)

    def __str__(self):
        return "{}-{}".format(self.serie.title, self.date)

    class Meta:
        verbose_name = "Marathon"
        verbose_name_plural = "Marathons"
        ordering = ['date']


class Episode(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    num = models.IntegerField()
    serie = models.ForeignKey(Serie, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def save(self):
        self.slug = slugify(self.title)
        super().save()

    class Meta:
        verbose_name = "Episode"
        verbose_name_plural = "Episodes"
        ordering = ['num']
