from django.contrib.auth.models import User
from django import forms
from .models import Serie, Marathon, Episode
import datetime


# class for SerieCreate
class SerieCreateForm(forms.ModelForm):
    class Meta:
        model = Serie
        fields = ['title', 'nb_eps', 'nb_eps_vu', 'synop', 'thumb']

    def clean(self):
        cleaned_data = super(SerieCreateForm, self).clean()
        n = cleaned_data.get("nb_eps")
        nv = cleaned_data.get("nb_eps_vu")

        if n <= 0 or nv < 0 or nv > n:
            raise forms.ValidationError("Error with nb_eps or nb_eps_vu!")
        return cleaned_data


# class for SerieUpdate
class SerieUpdateForm(forms.ModelForm):
    class Meta:
        model = Serie
        fields = ['nb_eps', 'nb_eps_vu', 'synop', 'thumb']

    def clean(self):
        cleaned_data = super(SerieUpdateForm, self).clean()
        n = cleaned_data.get("nb_eps")
        nv = cleaned_data.get("nb_eps_vu")

        if n <= 0 or nv < 0 or nv > n:
            raise forms.ValidationError("Error with nb_eps or nb_eps_vu!")
        return cleaned_data


# class for add marathon
class MarathonCreateForm(forms.ModelForm):
    class Meta:
        model = Marathon
        fields = ['date', 'place', 'is_validate']
        widgets = {
            'date': forms.DateTimeInput(attrs={'class': 'date', 'id': 'date'})
        }

# class for update episode


class EpisodeUpdateForm(forms.ModelForm):
    class Meta:
        model = Episode
        fields = ['title']
