from django.conf.urls import url
from . import views

app_name = 'series'
urlpatterns = [
    url(r'^$', views.SerieListView.as_view(), name="list"),
    url(r'^create_serie/$', views.SerieCreateView.as_view(), name="create_serie"),
    url(r'^marathons/$', views.MarathonListView.as_view(), name="marathons"),
    url(r'^(?P<slug>[\w-]+)/$',
        views.SerieDetailView.as_view(), name="detail"),
    url(r'^(?P<pk>[0-9]+)/validate/$', views.validate, name="validate"),
    url(r'^(?P<slug>[\w-]+)/create_marathon/$',
        views.MarathonCreateView.as_view(), name="create_marathon"),
    url(r'^(?P<slug>[\w-]+)/update/$',
        views.SerieUpdateView.as_view(), name="update"),
    url(r'^(?P<slug>[\w-]+)/update_episode/(?P<slug2>[\w-]+)/$',
        views.EpisodeUpdateView.as_view(), name="update_episode"),
    url(r'^(?P<slug>[\w-]+)/delete_marathon/(?P<pk>[0-9]+)/$',
        views.MarathonDeleteView.as_view(), name="delete_marathon"),
    url(r'^(?P<slug>[\w-]+)/delete_serie/$',
        views.SerieDeleteView.as_view(), name="delete_serie"),
]
