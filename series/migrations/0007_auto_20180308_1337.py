# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-08 12:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('series', '0006_auto_20180308_1208'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='episode',
            name='marathon',
        ),
        migrations.AddField(
            model_name='episode',
            name='is_seen',
            field=models.BooleanField(default=False),
        ),
    ]
