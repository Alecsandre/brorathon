from django.shortcuts import render,  get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse

from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Serie, Marathon, Episode
from .forms import SerieCreateForm, MarathonCreateForm, SerieUpdateForm, EpisodeUpdateForm
from .mixins import MessageMixin


class SerieListView(ListView):
    model = Serie
    template_name = 'series/index.html'
    context_object_name = 'series'


class SerieDetailView(LoginRequiredMixin, DetailView):
    model = Serie
    template_name = 'series/detail.html'


class SerieDeleteView(LoginRequiredMixin, MessageMixin, DeleteView):
    model = Serie
    success_url = reverse_lazy('home')
    success_message = 'The serie has been deleted successfully.'


class SerieCreateView(LoginRequiredMixin, MessageMixin, CreateView):
    form_class = SerieCreateForm
    template_name = 'series/create_serie.html'
    success_message = "The serie was created successfully."

    def form_valid(self, form):
        """ add the user to the instance """
        form.instance.add_by = self.request.user
        return super(SerieCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('series:detail', kwargs={'slug': self.object.slug})


class SerieUpdateView(LoginRequiredMixin, MessageMixin, UpdateView):
    model = Serie
    template_name = 'series/update.html'
    form_class = SerieUpdateForm
    success_message = "The serie was updated successfully."

    def get_success_url(self):
        return reverse_lazy('series:detail', kwargs={'slug': self.object.slug})


##############
#  MARATHON  #
##############

def validate(request, pk):
    marathon = get_object_or_404(Marathon, pk=pk)
    try:
        if marathon.is_validate:
            marathon.is_validate = False
        else:
            marathon.is_validate = True
        marathon.save()
    except (KeyError, Marathon.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})


class MarathonListView(LoginRequiredMixin, ListView):
    model = Marathon
    template_name = 'series/marathons.html'
    context_object_name = 'marathons'


class MarathonDeleteView(LoginRequiredMixin, MessageMixin, DeleteView):
    model = Marathon
    success_message = 'The marathon has been deleted successfully.'

    def get_object(self):
        serie = get_object_or_404(Serie, slug=self.kwargs['slug'])
        self.kwargs.update(
            {'pk': self.kwargs['pk'], 'serie': self.kwargs['slug']})
        return super(MarathonDeleteView, self).get_object()

    def get_success_url(self):
        return reverse_lazy('series:detail', kwargs={'slug': self.kwargs.get('serie')})


class MarathonCreateView(LoginRequiredMixin, MessageMixin, CreateView):
    form_class = MarathonCreateForm
    template_name = 'series/create_marathon.html'
    success_message = "The marathon was created successfully."

    def get_context_data(self, **kwargs):
        serie = get_object_or_404(Serie, slug=self.kwargs['slug'])
        kwargs.update({'serie': serie})
        return super(MarathonCreateView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        """ add the slug and user to the instance"""
        form.instance.add_by = self.request.user
        form.instance.serie = self.get_context_data()['serie']
        return super(MarathonCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('series:detail', kwargs=self.kwargs)


##############
#  EPISODES  #
##############

class EpisodeUpdateView(LoginRequiredMixin, MessageMixin, UpdateView):
    model = Episode
    template_name = 'series/update_episode.html'
    form_class = EpisodeUpdateForm
    success_message = "The episode was updated successfully."

    def get_context_data(self, **kwargs):
        serie = get_object_or_404(Serie, slug=self.kwargs['slug'])
        kwargs.update({'serie': serie, 'slug': self.kwargs['slug2']})
        return super(EpisodeUpdateView, self).get_context_data(**kwargs)

    def get_object(self):
        return get_object_or_404(Episode, slug=self.kwargs['slug2'])

    def get_success_url(self):
        return reverse_lazy('series:detail', kwargs={'slug': self.kwargs['slug']})


def add_disney():
    data = ['Snow White and the Seven Dwarfs (1937)',
            'Pinocchio (1940)',
            'Dumbo (1941)',
            'The Reluctant Dragon (1941)',
            'Bambi (1942)',
            'Victory Through Air Power (1943)',
            'Saludos Amigos (1943)',
            'The Three Caballeros (1944)',
            'Make Mine Music (1946)',
            'Song of the South (1946)',
            'Fun & Fancy Free (1947)',
            'Melody Time (1948)',
            'So Dear to My Heart (1948)',
            'The Adventures of Ichabod and Mr. Toad (1949)',
            'Cinderella (1950)',
            'Treasure Island (1950)',
            'Alice In Wonderland (1951)',
            'Story of Robin Hood and His Merrie Men, The (1952)',
            'Sword and the Rose, The (1953)',
            'Rob Roy: The Highland Rogue (1953)',
            'Peter Pan (1953)',
            '20,000 Leagues Under the Sea (1954)',
            'Littlest Outlaw, The ([1955])',
            'Lady and the Tramp (1955)',
            'Davy Crockett, King of the Wild Frontier (1955)',
            'Great Locomotive Chase, The (1956)',
            'Davy Crockett and the River Pirates (1956)',
            'Westward Ho, The Wagons! (1956)',
            'Johnny Tremain (1957)',
            'Old Yeller (1957)',
            'Light in the Forest, The (1958)',
            'Tonka (1958)',
            'Darby O\'Gill and the Little People (1959)',
            'Third Man on the Mountain (1959)',
            'Sleeping Beauty (1959)',
            'Shaggy Dog, The (1959)',
            'Ten Who Dared (1960)',
            'Swiss Family Robinson (1960)',
            'Toby Tyler (1960)',
            'Kidnapped (1960)',
            'Pollyanna (1960)',
            'Sign of Zorro, The (1960)',
            '101 Dalmatians (Animated) (1961)',
            'Babes in Toyland (1961)',
            'Parent Trap, The (1961)',
            'Greyfriars Bobby (1961)',
            'Absent-Minded Professor, The (1961)',
            'Bon Voyage! (1962)',
            'Moon Pilot (1962)',
            'Big Red (1962)',
            'Almost Angels (1962)',
            'Legend of Lobo, The (1962)',
            'In Search of Castaways (1962)',
            'Miracle of the White Stallions (1963)',
            'Savage Sam (1963)',
            'Incredible Journey, The (1963)',
            'Summer Magic (1963)',
            'Son of Flubber (1963)',
            'Sword in the Stone, The (1963)',
            'Emil and the Detectives (1964)',
            'Moon Spinners, The (1964)',
            'Three Lives of Thomasina, The (1964)',
            'Misadventures of Merlin Jones, The (1964)',
            'Tiger Walks, A (1964)',
            'Mary Poppins (1964)',
            'Those Calloways (1965)',
            'Monkey\'s Uncle, The (1965)',
            'Darn Cat, That (1965)',
            'Ugly Dachshund, The (1966)',
            'Follow Me, Boys! (1966)',
            'Fighting Prince of Donegal, The (1966)',
            'Lt. Robin Crusoe, U.S.N. (1966)',
            'Charlie, the Lonesome Cougar (1967)',
            'Gnome-Mobile, The (1967)',
            'Happiest Millionaire, The (1967)',
            'Monkeys, Go Home! (1967)',
            'Adventures of Bullwhip Griffin, The (1967)',
            'Jungle Book, The (1967)',
            'Horse in the Gray Flannel Suit, The (1968)',
            'One and Only, Genuine, Original Family Band, The (1968)',
            'Love Bug, The (1968)',
            'Blackbeard\'s Ghost (1968)',
            'Never A Dull Moment (1968)',
            'Smith! (1969)',
            'Rascal (1969)',
            'Computer Wore Tennis Shoes, The (1969)',
            'AristoCats, The (1970)',
            'Smoke (1970)',
            'Wild Country, The (1970)',
            'King of the Grizzlies (1970)',
            'Boatniks, The (1970)',
            'Barefoot Executive, The (1971)',
            'Scandalous John (1971)',
            'Bedknobs and Broomsticks (1971)',
            'Million Dollar Duck, The (1971)',
            'Now You See Him, Now You Don\'t(1972)',
            'Biscuit Eater, The (1972)',
            'Snowball Express (1972)',
            'Napoleon and Samantha (1972)',
            'Run, Cougar, Run (1972)',
            'Chandar, the Black Leopard of Ceylon (1972)',
            'World\'s Greatest Athlete, The (1973)',
            'One Little Indian (1973)',
            'Mustang (1973)',
            'Charley and the Angel (1973)',
            'Robin Hood (1973)',
            'Superdad (1973)',
            'Herbie Rides Again (1974)',
            'Bears and I, The (1974)',
            'Castaway Cowboy (1974)',
            'Island at the Top of the World, The (1974)',
            'Strongest Man in the World, The (1975)',
            'Apple Dumpling Gang, The (1975)',
            'Escape to Witch Mountain (1975)',
            'One of Our Dinosaurs is Missing (1975)',
            'Boy Who Talked to Badgers, The (1975)',
            'Ride A Wild Pony (1975)',
            'Treasure of Matecumbe (1976)',
            'Shaggy D.A., The (1976)',
            'No Deposit, No Return (1976)',
            'Freaky Friday (1976)',
            'Gus (1976)',
            'Pete\'s Dragon (1977)',
            'Herbie Goes to Monte Carlo (1977)',
            'Escape From The Dark (The Littlest Horse Thieves) (1977)',
            'Many Adventures of Winnie the Pooh, The (1977)',
            'Tale of Two Critters, A (1977)',
            'Rescuers, The (1977)',
            'Candleshoe (1977)',
            'Cat from Outer Space, The (1978)',
            'Return from Witch Mountain (1978)',
            'Hot Lead and Cold Feet (1978)',
            'Black Hole, The (1979)',
            'Muppet Movie, The (1979)',
            'Unidentified Flying Oddball (1979)',
            'Apple Dumpling Gang Rides Again (1979)',
            'North Avenue Irregulars, The (1979)',
            'Last Flight of Noah\'s Ark. The (1980)',
            'Midnight Madness (1980)',
            'Watcher in the Woods, The (1980)',
            'Herbie Goes Bananas (1980)',
            'Popeye (1980)',
            'Amy (1981)',
            'Fox and the Hound, The (1981)',
            'Condorman (1981)',
            'Devil and Max Devlin, The (1981)',
            'Dragonslayer (1981)',
            'Tron (1982)',
            'Night Crossing (1982)',
            'Tex (1982)',
            'Never Cry Wolf (1983)',
            'Trenchcoat (1983)',
            'Something Wicked This Way Comes (1983)',
            'Tiger Town (1983)',
            'Where the Toys Come From (1984)',
            'Love Leads the Way (1984)',
            'Black Cauldron, The (1985)',
            'Journey of Natty Gann, The (1985)',
            'Return to Oz (1985)',
            'One Magic Christmas (1985)',
            'Flight of the Navigator (1986)',
            'Basil - The Great Mouse Detective (1986)',
            'I-Man (1986)',
            'Brave Little Toaster, The (1987)',
            'Benji the Hunted (1987)',
            'Return to Snowy River (1988)',
            'Oliver and Company (1988)',
            'Honey, I Shrunk the Kids (1989)',
            'Little Mermaid, The (1989)',
            'Cheetah (1989)',
            'Rescuers Down Under, The (1990)',
            'Shipwrecked (1990)',
            'DuckTales: The Movie - Treasure of the Lost Lamp (1990)',
            'Rocketeer (1991)',
            'Beauty and the Beast (1991)',
            'Wild Hearts Can\'t Be Broken (1991)',
            'White Fang (1991)',
            'Aladdin (1992)',
            'Honey I Blew Up the Kid (1992)',
            'Newsies (1992)',
            'Mighty Ducks, The (1992)',
            'Muppet Christmas Carol, The (1992)',
            'Cool Runnings (1993)',
            'Heidi (1993)',
            'Adventures of Huck Finn, The (1993)',
            'Nightmare Before Christmas, The (1993)',
            'Hocus Pocus (1993)',
            'Three Musketeers, The (1993)',
            'Homeward Bound: The Incredible Journey (1993)',
            'Far Off Place, A (1993)',
            'White Fang 2 (1994)',
            'D2: The Mighty Ducks (1994)',
            'Angels in the Outfield (1994)',
            'Santa Clause, The (1994)',
            'Squanto: A Warrior\'s Tale (1994)',
            'Rudyard Kipling\'s The Jungle Book (1994)',
            'Blank Check (1994)',
            'Lion King, The (1994)',
            'Iron Will (1994)',
            'Man of the House (1995)',
            'Tall Tale (1995)',
            'Pocahontas (1995)',
            'Gordy (1995)',
            'Heavy Weights (1995)',
            'Operation Dumbo Drop (1995)',
            'Big Green, The (1995)',
            'Kid in King Arthurv\'s Court, A (1995)',
            'Tom and Huck (1995)',
            'Goofy Movie, A (1995)',
            'Toy Story (1995)',
            'Homeward Bound II: Lost in San Francisco (1996)',
            'James and the Giant Peach (1996)',
            'Muppet Treasure Island (1996)',
            '101 Dalmatians (1996)',
            'First Kid (1996)',
            'Hunchback of Notre Dame, The (1996)',
            'D3: The Mighty Ducks (1996)',
            'Tower of Terror (1997)',
            'Darn Cat, That (1997)',
            'Angels in the Endzone (1997)',
            'Oliver Twist (1997)',
            'George of the Jungle (1997)',
            'Hercules (1997)',
            'Mr. Magoo (1997)',
            'Flubber (1997)',
            'Beauty and the Beast: The Enchanted Christmas (1997)',
            'Jungle 2 Jungle (1997)',
            'Rocket Man (1997)',
            'Under Wraps (1997)',
            'Northern Lights (1997)',
            'Air Bud (1997)',
            'Knight In Camelot, A (1998)',
            'Mulan (1998)',
            'Meet the Deedles (1998)',
            'Pocahontas II: Journey to a New World (1998)',
            'Halloweentown (1998)',
            'You Lucky Dog (1998)',
            'Wonderful Ice Cream Suit, The (1998)',
            'Parent Trap, The (new) (1998)',
            'I\'ll Be Home for Christmas (1998)',
            'Mighty Joe Young (1998)',
            'Brink! (1998)',
            'Bugs Life, A (1998)',
            'Lion King II: Simbas Pride, The (1998)',
            'Air Bud: Golden Receiver (1998)',
            'My Favorite Martian (1999)',
            'Horse Sense (1999)',
            'Zenon: Girl of the 21st Century (1999)',
            'Can of Worms (1999)',
            'Johnny Tsunami (1999)',
            'Annie (1999)',
            'Genius (1999)',
            'Don\'t Look Under The Bed (1999)',
            'Thirteenth Year, The (1999)',
            'Endurance (1999)',
            'Straight Story, The (1999)',
            'Doug\'s 1st Movie (1999)',
            'Tarzan (1999)',
            'Inspector Gadget (1999)',
            'H-E Double Hockey Sticks (1999)',
            'Mickey\'s Once Upon a Christmas (1999)',
            'Fantasia 2000 (1999)',
            'Toy Story 2 (1999)',
            'Smart House (1999)',
            'Dinosaur (2000)',
            'Whispers: An Elephant\'s Tale (2000)',
            'Kid, The (2000)',
            'Air Bud 3: World Pup (2000)',
            'Extremely Goofy Movie, An (2000)',
            'Little Mermaid II, The: Return to the Sea (2000)',
            'Color of Friendship, The (2000)',
            '102 Dalmatians (2000)',
            'Mail to the Chief (2000)',
            'Geppetto (2000)',
            'Emperor\'s New Groove, The (2000)',
            'Tigger Movie, The (2000)',
            'Up, Up, and Away (2000)',
            'Remember the Titans (2000)',
            'Alley Cats Strike (2000)',
            'Ready to Run (2000)',
            'Miracle in Lane 2 (2000)',
            'Stepsister From Planet Weird (2000)',
            'Quints (2000)',
            'Other Me, The (2000)',
            'Mom\'s Got A Date With A Vampire (2000)',
            'Phantom of the Megaplex (2000)',
            'Ultimate Christmas Present, The (2000)',
            'Angels In The Infield (2000)',
            'Rip Girls (2000)',
            'Princess Diaries, The (2001)',
            'Atlantis: The Lost Empire (2001)',
            'Lady and the Tramp II: Scamp\'s Adventure (2001)',
            'Mickey\'s Magical Christmas: Snowed in at the House of Mouse (2001)',
            'Motocrossed (2001)',
            'Max Keeble\'s Big Move (2001)',
            'Jett Jackson: The Movie (2001)',
            'Recess: School\'s Out (2001)',
            'Monsters, Inc (2001)',
            'Poof Point, The (2001)',
            'Other Side of Heaven, The (2001)',
            'Halloweentown II: Kalabar\'s Revenge (2001)',
            'Jumping Ship (2001)',
            'Jennie Project, The (2001)',
            'Hounded (2001)',
            'Luck of the Irish (2001)',
            'Zenon: The Zequel (2001)',
            'Prom (2001)',
            'Twas the Night (2001)',
            'Air Bud: Seventh Inning Fetch (2002)',
            'Ring of Endless Light, A (2002)',
            'Gotta Kick It Up! (2002)',
            'Get a Clue (2002)',
            'Tru Confessions (2002)',
            'Scream Team, The (2002)',
            'Cadet Kelly (2002)',
            'Double Teamed (2002)',
            'Snow Dogs (2002)',
            'Tuck Everlasting (2002)',
            'Cinderella II: Dreams Come True (2002)',
            'Lilo & Stitch (2002)',
            'Country Bears, The (2002)',
            'Santa Clause 2: The Mrs. Clause (2002)',
            'Treasure Planet (2002)',
            'Winnie the Pooh: A Very Merry Christmas (2002)',
            'Rookie, The (2002)',
            'Return to Never Land (2002)',
            'Brother Bear (2003)',
            'Air Bud Spikes Back (2003)',
            'You Wish! (2003)',
            'Right On Track (2003)',
            'Even Stevens Movie, The (2003)',
            'Kim Possible: A Sitch in Time (2003)',
            'Cheetah Girls, The (2003)',
            'Full-Court Miracle (2003)',
            '101 Dalmatians 2: Patch\'s London Adventure (2003)',
            'Eddie\'s Million Dollar Cook-Off (2003)',
            'Jungle Book 2, The (2003)',
            'Inspector Gadget 2 (2003)',
            'Piglet\'s Big Movie (2003)',
            'Young Black Stallion, The (2003)',
            'Haunted Mansion, The (2003)',
            'George of the Jungle 2 (2003)',
            'Freaky Friday (2003)',
            'Finding Nemo (2003)',
            'Lizzie McGuire Movie (2003)',
            'Atlantis: Milo\'s Return (2003)',
            'Holes (2003)',
            'Pirates of the Caribbean: The Curse of the Black Pearl (2003)',
            'Confessions of a Teenage Drama Queen (2004)',
            'Miracle (2004)',
            'Teacher\'s Pet (2004)',
            'Stuck in the Suburbs (2004)',
            'Going to the Mat (2004)',
            'Halloweentown High (2004)',
            'Pixel Perfect (2004)',
            'Home on the Range (2004)',
            'Tiger Cruise (2004)',
            'Winnie the Pooh: Springtime with Roo (2004)',
            'Zenon: Z3 (2004)',
            'Three Musketeers, The (Animated) (2004)',
            'Princess Diaries 2: Royal Engagement (2004)',
            'Howl\'s Moving Castle (2004)',
            'Mulan II (2004)',
            'National Treasure (2004)',
            'Mickey\'s Twice Upon a Christmas (2004)',
            'Incredibles, The (2004)',
            'Lion King 1 1/2, The (2004)',
            'Around the World in 80 Days (2004)',
            'Pooh\'s Heffalump Movie (2005)',
            'Lilo and Stitch 2: Stich Has a Glitch (2005)',
            'Greatest Game Ever Played, The (2005)',
            'Twitches (2005)',
            'Emperor\'s New Groove 2: Kronk\'s New Groove (2005)',
            'Kim Possible Movie: So the Drama (2005)',
            'Life is Ruff (2005)',
            'Go Figure (2005)',
            'Once Upon a Mattress (2005)',
            'Proud Family Movie, The (2005)',
            'Pacifier, The (2005)',
            'Chronicles of Narnia: The Lion, Witch and the Wardrobe (2005)',
            'Sky High (2005)',
            'Pooh\'s Heffalump Halloween Movie (2005)',
            'Kronk\'s New Groove (2005)',
            'Herbie Fully Loaded (2005)',
            'Chicken Little (2005)',
            'Valiant (2005)',
            'Buffalo Dreams (2005)',
            'Now You See It (2005)',
            'Ice Princess (2005)',
            'Read It And Weep (2006)',
            'Wendy Wu: Homecoming Warrior (2006)',
            'Cow Belles (2006)',
            'Cheetah Girls 2, The (2006)',
            'Air Buddies (2006)',
            'Return to Halloweentown (2006)',
            'High School Musical (2006)',
            'Fox and the Hound 2, The (2006)',
            'Glory Road (2006)',
            'Brother Bear 2 (2006)',
            'Eight Below (2006)',
            'Shaggy Dog, The (2006)',
            'Wild, The (2006)',
            'Pirates of the Caribbean: Dead Man\'s Chest (2006)',
            'Invincible (2006)',
            'Santa Clause 3: The Escape Clause (2006)',
            'Cars (2006)',
            'Bambi II (2006)',
            'National Treasure: Book of Secrets (2007)',
            'High School Musical 2 (2007)',
            'Twitches Too (2007)',
            'Secret of the Magic Gourd, The (2007)',
            'Enchanted (2007)',
            'My Friends Tigger and Pooh: Super Sleuth Christmas (2007)',
            'Ratatouille (2007)',
            'Game Plan, The (2007)',
            'Underdog (2007)',
            'Cinderella III: A Twist in Time (2007)',
            'Meet the Robinsons (2007)',
            'Pirates of the Caribbean: At World\'s End (2007)',
            'Jump In! (2007)',
            'Johnny Kapahala: Back on Board (2007)',
            'Bridge to Terabithia (2007)',
            'Little Mermaid III - Ariel\'s Beginning (2008)',
            'Tinker Bell (2008)',
            'Bolt (2008)',
            'Camp Rock (2008)',
            'Minutemen (2008)',
            'Snow Buddies (2008)',
            'Beverly Hills Chihuahua (2008)',
            'Wall E (2008)',
            'High School Musical 3: Senior Year (2008)',
            'Chronicles of Narnia: Prince Caspian (2008)',
            'Cheetah Girls, The: One World (2008)',
            'Bedtime Stories (2008)',
            'College Road Trip (2008)',
            'Wizards of Waverly Place: The Movie (2009)',
            'Santa Buddies (2009)',
            'Tigger & Pooh and a Musical Too (2009)',
            'Dadnapped (2009)',
            'G-Force (2009)',
            'Space Buddies (2009)',
            'Princess Protection Program (2009)',
            'Christmas Carol, A (2009)',
            'Hatching Pete (2009)',
            'Old Dogs (2009)',
            'Ponyo (2009)',
            'Princess and the Frog, The (2009)',
            'Up (2009)',
            'Hannah Montana: The Movie (2009)',
            'Race to Witch Mountain (2009)',
            'Tinker Bell and the Lost Treasure (2009)',
            'Toy Story 3 (2010)',
            'Search for Santa Paws, The (2010)',
            'Avalon High (2010)',
            'TRON Legacy (2010)',
            'Alice In Wonderland (2010)',
            'Den Brother (2010)',
            'Prince of Persia : Sands of Time (2010)',
            'Secretariat (2010)',
            'Tangled (2010)',
            'Tinker Bell and the Great Fairy Rescue (2010)',
            'Sorcerers Apprentice (2010)',
            'Star Struck (2010)',
            'Camp Rock 2: The Final Jam (2010)',
            'Mars Needs Moms (2011)',
            'Geek Charming (2011)',
            'Good Luck Charlie, It\'s Christmas (2011)',
            'Phineas & Ferb: Across the 2nd Dimension (2011)',
            'Cars 2 (2011)',
            'Pirates of the Caribbean: On Stranger Tides (2011)',
            'Spooky Buddies (2011)',
            'Muppets, The (2011)',
            'Once Upon A Warrior (Anaganaga O Dheerudu) (2011)',
            'Suite Life Movie, The (2011)',
            'Sharpay\'s Fabulous Adventure (2011)',
            'Lemonade Mouth (2011)',
            'Winnie the Pooh (2011)',
            'Beverly Hills Chihuahua 2 (2011)',
            'Let It Shine (2012)',
            'Sofia the First: Once Upon a Princess (2012)',
            'Girl vs. Monster (2012)',
            'Santa Paws 2: The Santa Pups (2012)',
            'Beverly Hills Chihuahua 3: Viva La Fiesta! (2012)',
            'Tinker Bell: Secret of the Wings (2012)',
            'Treasure Buddies (2012)',
            'Radio Rebel (2012)',
            'Frankenweenie (2012)',
            'Wreck-It Ralph (2012)',
            'Brave (2012)',
            'Frenemies (2012)',
            'Secret World of Arrietty, The (2012)',
            'Odd Life of Timothy Green, The (2012)',
            'John Carter (2012)',
            'Teen Beach Movie (2013)',
            'Lone Ranger (2013)',
            'Oz: The Great and Powerful (2013)',
            'Frozen (2013)',
            'Super Buddies (2013)',
            'Planes (2013)',
            'Saving Mr. Banks (2013)',
            'Monsters University (2013)',
            'Cloud 9 (2014)',
            'How To Build A Better Boy (2014)',
            'Zapped (2014)',
            'Into The Woods (2014)',
            'Alexander and the Terrible, Horrible, No Good, Very Bad Day (2014)',
            'Million Dollar Arm (2014)',
            'Pirate Fairy, The (2014)',
            'Big Hero 6 (2014)',
            'Muppets: Most Wanted (2014)',
            'Maleficent (2014)',
            'McFarland, USA (2014)',
            'Planes: Fire Rescue (2014)',
            'Teen Beach Movie 2 (2015)',
            'Bad Hair Day (2015)',
            'Tinkerbell and the Legend of the Neverbeast (2015)',
            'Descendants (2015)',
            'Tomorrowland (2015)',
            'Inside Out (2015)',
            'Cinderella (2015)',
            'Good Dinosaur, The (2015)',
            'Moana (2016)',
            'Swap, The (2016)',
            'Finest Hours, The (2016)',
            'Pete\'s Dragon (2016)',
            'Adventures in Babysitting (2016)',
            'Queen of Katwe (2016)',
            'Finding Dory (2016)',
            'Zootopia (2016)',
            'Alice Through the Looking Glass (2016)',
            'Jungle Book (2016)',
            'The BFG (2016)',
            'Coco (2017)',
            'Pirates of the Caribbean: Dead Men Tell No Tales (2017)',
            'Cars 3 (2017)',
            'Beauty and the Beast (2017)',
            'Descendants 2 (2017)',
            'Wreck-It Ralph 2: Ralph Breaks the Internet (2018)',
            'Incredibles 2 (2018)',
            'Cruella (Live-Action) (2018)',
            'Mary Poppins Returns (2018)',
            'Mulan (Live-Action) (2018)',
            'Magic Camp (2018)',
            'A Wrinkle in Time (2018)']

    i = 1
    for f in data:
        try:
            Episode.objects.get(title=f)
        except:
            print(f)
            Episode(title=f, num=i, serie=Serie.objects.get(
                slug='disneypixar')).save()
        i += 1
