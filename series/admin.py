from django.contrib import admin
from .models import Serie, Marathon, Episode


@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ("title", "add_by")
    prepopulated_fields = {"slug": ("title",)}


@admin.register(Marathon)
class MarathonAdmin(admin.ModelAdmin):
    pass


@admin.register(Episode)
class EpisodeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
