from django.conf.urls import url, include
from django.contrib import admin
from series.views import SerieListView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^series/', include('series.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^$', SerieListView.as_view(), name="home"),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
