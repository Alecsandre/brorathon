from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages

from django.contrib.auth.mixins import LoginRequiredMixin
from series.mixins import MessageMixin


class Signup(MessageMixin, CreateView):
    form_class = UserCreationForm
    template_name = 'accounts/signup.html'
    success_url = reverse_lazy('home')
    success_message = 'You are now sign in.'


class Login(MessageMixin, LoginView):
    template_name = 'accounts/login.html'
    success_message = 'You have been logged in successfully.'


class Logout(LogoutView):
    success_message = 'You have been logged out successfully.'

    def get_next_page(self):
        messages.success(self.request, self.success_message)
        return super(Logout, self).get_next_page()
