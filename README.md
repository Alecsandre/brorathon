# Description

Template [django 1.11](https://docs.djangoproject.com/en/1.11/) et [heroku](https://heroku.com).

# Dépendances

- [python3.6](https://docs.python.org/3.6/).
- Heroku Cli
- Heroku account

# Installation

## 1) Python

- `virtualenv venv` (.env si _linux_)
- `.\venv\Scripts\activate` ou `source .env/bin/activate`(_linux_)
- `pip install -r requirements.txt`

## 2) Heroku

- `git init` seulement si il n'y a pas de repo
- `git add .`
- `git commit -m "Initial commit heroku"`
- `heroku login`: se connecter avec ses identifiants
- `heroku create`: créer un projet
- `git push heroku master` (Si vous avez `Error while running '$ python manage.py collectstatic --noinput'.` comme erreur) utiliser `heroku config:set DISABLE_COLLECTSTATIC=1`
- récupérer votre `Database Credentials` pour le projet sur votre dashboard heroku et remplisser les dans le `settings.py` puis _push_ le tout

## 3) Django

(Comme la bd est gérée par heroku toutes les commandes en relations sont exécuter avec `heroku run` devant. Personellement j'ai fais un alias :) )

- `heroku run python manage.py makemigrations`: Convertir les modèles ORM en requêtes SQL
- `heroku run pyhton managa.py migrate`: Lancer les requetes sql (CRUD tables de la BD)
- `heroku run python manage.py createsuperuser`: Créer le superuser

# Lancement

Le serveur heroku-django se lance via `heroku open`. Vous pouvez régler le nombre d'instance avec `heroku ps:scale web=1` pour une instance.
